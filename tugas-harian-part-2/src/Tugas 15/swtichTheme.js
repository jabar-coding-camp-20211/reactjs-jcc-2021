import React, { useContext } from "react"
import { ThemeContext } from "../Context/themeContext"
import { Switch } from 'antd';

const ButtonSwitch = () => {

    const {theme, setTheme} = useContext(ThemeContext)

    const handleChangeTheme = () => {
        setTheme( theme === "light" ? "dark" : "light" )
    }

    return(
        <>
            <div style={{width:"100%", display:"flex", justifyContent:"center", alignItems:"center", marginTop:"20px"}}>
                <Switch onClick={handleChangeTheme} checkedChildren="Light" unCheckedChildren="Dark" defaultChecked />
                {/* <button onClick={handleChangeTheme} style={{padding:"7px", borderRadius:"10px"}}>Change Navbar to Dark</button> */}
            </div>
        </>
    )
}

export default ButtonSwitch