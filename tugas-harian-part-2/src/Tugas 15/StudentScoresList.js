import React, { useContext, useEffect } from "react"
import { useHistory } from "react-router"
import { NilaiMahasiswaContext } from "../Context/nilaiMahasiswaContext"
import { Button, Table, Tag, Space } from "antd"
import { DeleteFilled, EditFilled } from '@ant-design/icons';

const StudentScoresList = () => {
    let history = useHistory()


    const { input, setInput, dataMahasiswa, functions, fetchStatus, setFetchStatus } = useContext(NilaiMahasiswaContext)
    const { fetchData, getScore, functionDelete, functionEdit } = functions

    useEffect(() => {

        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }

    }, [fetchData, fetchStatus, setFetchStatus])



    const handleDelete = (event) => {
        let idMahasiswa = parseInt(event.currentTarget.value)

        functionDelete(idMahasiswa)
    }

    const handleEdit = (event) => {
        let idMahasiswa = parseInt(event.currentTarget.value)
        history.push(`/tugas15/edit/${idMahasiswa}`)
        // functionEdit(idMahasiswa)
    }

    const handleCreate = () => {
        history.push('/tugas15/create')
        setInput({
            nama: "",
            course: "",
            score: 0
        })
    }

    console.log(dataMahasiswa)
    const columns = [
        // {
        //     title: 'No',
        //     // dataIndex: 'no',
        //     // render: (e,index) => {
        //     //     <p>{index}</p>
        //     // }
        // },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Mata Kuliah',
            dataIndex: 'course',
            key: 'course',
        },
        {
            title: 'Nilai',
            dataIndex: 'score',
            key: 'score',
        },
        {
            title: 'Index Nilai',
            dataIndex: 'indexScore',
            key: 'indexScore',
        },
        {
            title: 'Action',
            key: 'action',
            render: (res, index) => (
                <>
                    <Button icon={<EditFilled />} onClick={handleEdit} value={res.id} />
                    <Button icon={<DeleteFilled />} danger type="primary" onClick={handleDelete} value={res.id} style={{ marginLeft: "10px" }} />
                </>
            ),
        },
    ];

    const data = dataMahasiswa

    return (
        <>
            <br />
            <h1 style={{ textAlign: "center" }}>Daftar Nilai Mahasiswa</h1>
            <Button type="primary" onClick={handleCreate} style={{ marginLeft: "320px", padding: "5px", marginTop: "10px" }}>Tambah Data Nilai Mahasiswa</Button>
            <Table columns={columns} dataSource={data} className="table-list" />
            <br />
            <br />


        </>
    )
}

export default StudentScoresList