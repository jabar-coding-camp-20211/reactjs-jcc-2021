import axios from "axios"
import React, { useEffect, useState } from "react"

const Tugas12 = () => {

    const [dataMahasiswa, setDataMahasiswa] = useState([])
    const [input, setInput] = useState({
        nama: "",
        course: "",
        score: 0
    })
    const [fetchStatus, setFetchStatus] = useState(true)
    const [currentId, setCurrentId] = useState(-1)

    useEffect(() => {

        const fetchData = async () => {
            let result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            let data = result.data
            console.log(data)
            setDataMahasiswa(data.map((e) => {
                return {
                    id: e.id,
                    name: e.name,
                    score: e.score,
                    course: e.course
                }
            }))
        }

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }

    }, [ fetchStatus, setFetchStatus])


    const getScore = (score) => {
        if (score >= 80) {
            return "A"
        } else if (score >= 70 && score < 80) {
            return "B"
        } else if (score >= 60 && score < 70) {
            return "C"
        } else if (score >= 50 && score < 60) {
            return "D"
        } else {
            return "E"
        }
    }

    const handleChange = (event) => {
        let typeOfValue = event.target.value
        let name = event.target.name

        setInput({ ...input, [name]: typeOfValue })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)

        if (currentId === -1) {
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, {
                name: input.nama,
                course: input.course,
                score: input.score,
            }).then((res) => {
               setFetchStatus(true)
            })
        }else{
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {
                name: input.nama,
                course: input.course,
                score: input.score,
            }).then((res) => {
                setFetchStatus(true)
            })
        }

        setInput({
            nama: "",
            course: "",
            score: 0
        })  
        setCurrentId(-1)
    }

    const handleDelete = (event) => {
        let idMahasiswa = parseInt(event.target.value)

        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
            .then(() => {
                setFetchStatus(true)
            })
    }

    const handleEdit = (event) => {
        let idMahasiswa = parseInt(event.target.value)

        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`)
            .then((res) => {
                let data = res.data
                setInput({
                    id: data.id,
                    nama: data.name,
                    course: data.course,
                    score: data.score
                })
                setCurrentId(data.id)
            })
    }

    return (
        <>
            <br />
            <h1 style={{ textAlign: "center" }}>Daftar Nilai Mahasiswa</h1>
            <table id="customers">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Mata Kuliah</th>
                        <th>Nilai</th>
                        <th>Index Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataMahasiswa !== null && (
                        <>
                            {dataMahasiswa.map((e, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{e.name}</td>
                                        <td>{e.course}</td>
                                        <td>{e.score}</td>
                                        <td>{getScore(e.score)}</td>
                                        <td>
                                            <button onClick={handleEdit} value={e.id}>edit</button>
                                            <button onClick={handleDelete} value={e.id} style={{ marginLeft: "10px" }}>delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </>
                    )}
                </tbody>
            </table>
            <br />
            <br />
            <h1 style={{ textAlign: "center" }}>Form Daftar Nilai Mahasiswa</h1>
            <br />
            <form method="post" onSubmit={handleSubmit} style={{ width: "50%", border: "1px solid #aaa", margin: "auto", padding: "50px", marginBottom: "20px" }}>
                <strong style={{ width: "300px", display: "inline-block" }}>Nama : </strong>
                <input style={{ float: "right" }} onChange={handleChange} value={input.nama} name="nama" type="text" required />
                <br />
                <br />
                <strong style={{ width: "300px", display: "inline-block" }}>Mata Kuliah : </strong>
                <input style={{ float: "right" }} onChange={handleChange} value={input.course} name="course" type="text" required />
                <br />
                <br />
                <strong style={{ width: "300px", display: "inline-block" }}>Nilai : </strong>
                <input style={{ float: "right" }} onChange={handleChange} value={input.score} name="score" type="number" required min={0} max={100} />
                <br />
                <br />
                <input style={{ float: "right" }} type="submit" />
            </form>
        </>
    )
}

export default Tugas12