import axios from 'axios';
import React, { isValidElement, useState } from 'react';
import { useEffect } from 'react/cjs/react.development';
import Gambar from "../src/assets/img/logo.png"

const App = () => {

  const [data, setData] = useState([])
  const [input, setInput] = useState({
    name: "",
    search: "",
    is_android_app: true,
    is_ios_app: true
  })
  const [trigger, setTrigger] = useState(true)

  useEffect(() => {
    const fetchData = async () => {
      let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
      console.log(result.data)
      setData(result.data.map((e) => {
        return {
          id: e.id,
          name: e.name,
          is_android_app: e.is_android_app,
          is_ios_app: e.is_ios_app
        }
      }))
    }

    if (trigger) {
      fetchData()
      setTrigger(false)
    }
  }, [trigger])


  const submitSearch = (event) => {
    event.preventDefault()
    console.log(input)

    if (input !== "") {

      const searchData = async () => {
        let resultFilter = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        let filterData = resultFilter.data.filter((e) => {
          return Object.values(e).join(" ").toLowerCase().includes(input.toLowerCase())
        })
        setData(filterData)
      }

      searchData()
      // setTrigger(true)
    } else {
      setTrigger(true)
    }
  }



  const handleChange = (event) => {
    let value = event.target.value
    let name = event.target.name
    let platfrom = ["is_android_app", "is_ios_app"]
    console.log(input[name])
    console.log({ [name] : input[name]})
    if (platfrom.indexOf(name) < 0) {
      setInput({ ...input, [name]: value })
    }else{
      setInput({...input, [name] : !input[name]})
    }

  }

  const handleSubmit = (event) => {
    event.preventDefault()

    console.log(input)
  }
  return (
    <>
      <form method="post" onSubmit={submitSearch}>
        <input name="search" type="text" className="search-bar" onChange={handleChange} value={input.search} />
      </form>
      <div>
        {data !== null ? <>
          {data.map((e) => {
            return (
              <ul key={e.id}>
                <li>{e.name} | {e.is_android_app ? "android" : ''} {e.is_ios_app ? "ios" : ''}</li>
              </ul>
            )
          })}
        </> : null}
      </div>

      <form onSubmit={handleSubmit}>
        <input type="text" name="name" value={input.name} onChange={handleChange} />
        <div>
          Platfrom :
          <input style={{ display: "inline-block", cursor: "pointer", marginLeft: "10px" }} type="checkbox" name="is_android_app" checked={input.is_android_app} onChange={handleChange} />
          <strong>Android</strong>
          <input style={{ display: "inline-block", cursor: "pointer", marginLeft: "10px" }} type="checkbox" name="is_ios_app" checked={input.is_ios_app} onChange={handleChange} />
          <strong>iOS</strong>
          <br />
        </div>
      </form>
    </>
  );

}

export default App;