import React, { useState } from "react"

const Tugas11 = () => {

    const [dataBuah, setDataBuah] = useState([
        { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
        { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
        { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
        { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 }
    ])

    const [input, setInput] = useState({
        nama: "",
        hargaTotal: 0,
        beratTotal: 0
    })

    const [currentIndex, setCurrentIndex] = useState(-1)
    console.log(currentIndex)
    const handleChange = (event) => {
        let typeOfValue = event.target.value
        let name = event.target.name

        setInput({...input, [name] : typeOfValue})

    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)

        let nama = input.nama
        let hargaTotal = input.hargaTotal
        let beratTotal = input.beratTotal

        let newData = dataBuah

        if(currentIndex === -1){
            newData = [...dataBuah, {
                nama ,
                hargaTotal,
                beratTotal,
            }]
        }else{
            newData[currentIndex] = {
                nama ,
                hargaTotal,
                beratTotal,
            }
        }

        setDataBuah(newData)
        setInput({
            nama: "",
            hargaTotal: 0,
            beratTotal: 0
        })
        setCurrentIndex(-1)
    }
 
    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = dataBuah[index]
        let newData = dataBuah.filter((e) => {return e !== deletedItem})
        setDataBuah(newData)

    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editdItem = dataBuah[index]
        setInput({
            ...input,
            nama : editdItem.nama,
            hargaTotal : editdItem.hargaTotal,
            beratTotal : editdItem.beratTotal,
        })
        setCurrentIndex(index)

    }

    return (

        <>
            <br />
            <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
            <table id="customers">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga Total</th>
                        <th>Berat Total</th>
                        <th>Harga per Kg</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {dataBuah !== null && (
                        <>
                            {dataBuah.map((e, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{e.nama}</td>
                                        <td>{e.hargaTotal}</td>
                                        <td>{e.beratTotal/1000} Kg</td>
                                        <td>{ e.hargaTotal/(e.beratTotal/1000) }</td>
                                        <td>
                                            <button onClick={handleEdit} value={index}>edit</button>
                                            <button onClick={handleDelete} value={index} style={{marginLeft:"10px"}}>delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </>
                    )}
                </tbody>
            </table>
            <br/>
            <br/>
            <h1 style={{textAlign:"center"}}>Form Daftar Harga Buah</h1>
            <br/>
            <form onSubmit={handleSubmit} style={{width:"50%", border:"1px solid #aaa", margin:"auto", padding:"50px", marginBottom:"20px"}}>
                <strong style={{width:"300px", display:"inline-block"}}>Nama : </strong>
                <input style={{float:"right"}} onChange={handleChange} value={input.nama} name="nama" type="text" required/>
                <br/>
                <br/>
                <strong  style={{width:"300px",display:"inline-block"}}>Harga Total : </strong>
                <input style={{float:"right"}} onChange={handleChange} value={input.hargaTotal} name="hargaTotal" type="number" required/>
                <br/>
                <br/>
                <strong  style={{width:"300px",display:"inline-block"}}>Berat Total ( dalam gram ) : </strong>
                <input style={{float:"right"}} onChange={handleChange} value={input.beratTotal} name="beratTotal" type="number" required min={2000}/>
                <br/>
                <br/>
                <input style={{ float:"right"}} type="submit"/>
            </form>
        </>
    )
}

export default Tugas11