import axios from "axios"
import React, { useContext, useEffect } from "react"
import { useParams } from "react-router"
import { MobileAppContext } from "../Context/MobileAppContext"

const MobileForm = () => {
    let {Id} = useParams()
    const { data, setData, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions} = useContext(MobileAppContext)
    
    const { handleChange, functionSubmit, functionUpdate, functionEdit } = functions

    useEffect(() => {
        if(Id !== undefined){
            functionEdit(Id)
        }
    },[])

    const handleSubmit = (e) => {
        e.preventDefault()
        if (currentId === null) {
           functionSubmit()
        }else{
           functionUpdate(currentId)
        }

        setInput({
            category: "",
            description: "",
            image_url: "",
            is_android_app: true,
            is_ios_app: true,
            name: "",
            price: 0,
            rating: 0,
            release_year: 2007,
            size: 0
        })
        setCurrentId(null)
    }


    return (
        <>
            <div className="form">
                <form method="post" onSubmit={handleSubmit}>
                    <label >Name</label>
                    <input onChange={handleChange} required className="input-form" type="text" id="fname" name="name" placeholder="Your name.." value={input.name} />

                    <label >Category</label>
                    <input onChange={handleChange} required className="input-form" type="text" id="fname" name="category" value={input.category} />

                    <label >Description</label>
                    <textarea onChange={handleChange} required className="input-form" type="text" id="lname" name="description" placeholder="Your last name.." value={input.description} />

                    <label >year</label>
                    <input onChange={handleChange} required className="input-form" type="number" id="lname" name="release_year" min={2007} max={2021} value={input.release_year} />

                    <label >size</label>
                    <input onChange={handleChange} required className="input-form" type="number" id="lname" name="size" value={input.size} />

                    <label >price</label>
                    <input onChange={handleChange} required className="input-form" type="number" id="lname" name="price" value={input.price} />

                    <label >rating</label>
                    <input onChange={handleChange} required className="input-form" type="number" id="lname" name="rating" min={0} max={5} value={input.rating} />

                    <label >img url</label>
                    <input onChange={handleChange} required className="input-form" type="text" id="lname" name="image_url" value={input.image_url} />

                    <label >Plarform</label>
                    <br />
                    <label style={{ display: "block", width: "150px" }}>
                        <b>Android :</b>
                        <input style={{ display: "inline-block" }} onChange={handleChange} type="checkbox" id="lname" name="is_android_app" checked={input.is_android_app} />
                    </label>

                    <label style={{ display: "block", width: "150px" }}>
                        <b>IOS :</b>
                        <input style={{ display: "inline-block" }} onChange={handleChange} type="checkbox" id="lname" name="is_ios_app" checked={input.is_ios_app} />
                    </label>


                    <br />
                    <input className="input-submit" type="submit" value="Submit" />
                </form>
            </div>

        </>
    )
}

export default MobileForm